package com.nonagon.melladesk.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
/**
 * @ClassName:
 * @author: flitsneak nonagon
 * @date: 2021/1/29
 */
@Data
public class UserVO implements Serializable {
    /**
     * 用户编号
     */
    private String userId;
    /**
     * 设置编号
     */
    private Integer userSettingId;
    /**
     * 图片编号
     */
    private Integer imageId;
    /**
     * 图片地址
     */
    private String url;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 哈希字符串
     */
    private String hash;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 名
     */
    private String firstName;
    /**
     * 姓
     */
    private String lastName;
    /**
     * 国家
     */
    private String country;
    /**
     * 城市
     */
    private String city;
    /**
     * 州
     */
    private String state;
    /**
     * 邮政编码
     */
    private Integer zipcode;
    /**
     * 地址1
     */
    private String address1;
    /**
     *
     */
    private String address2;
    /**
     * 是否被删除，1删除，0未删除
     */
    private Integer isDeleted;
    /**
     * 1=封停账号，0=未封号
     */
    private Integer isLimit;
    /**
     * 创建时间
     */
    private LocalDate createTime;
    /**
     * 登录方式id
     */
    private Integer identityTypeId;

}
