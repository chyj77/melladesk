package com.nonagon.melladesk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MelladeskApplication {

    public static void main(String[] args) {
        SpringApplication.run(MelladeskApplication.class, args);
    }

}
