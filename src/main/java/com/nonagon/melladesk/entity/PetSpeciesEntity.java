package com.nonagon.melladesk.entity;

import lombok.Data;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import java.io.Serializable;

/**
 * 宠物的物种表，猫，狗，马，兔等
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:13
 */
@Data
@Table("pet_species")
public class PetSpeciesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 宠物物种编号
	 */
	@Id
	private Integer petSpeciesId;
	/**
	 * 宠物物种图片
	 */
	private Integer imageId;
	/**
	 * 物种的名字
	 */
	private String speciesName;

}
