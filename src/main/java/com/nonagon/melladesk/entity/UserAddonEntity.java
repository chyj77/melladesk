package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 用户表和插件表的中间表，一个用户可以对应多个插件吗，以及插件的一些基本信息
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:13
 */
@Data
@Table("user__addon")
public class UserAddonEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户和用户插件中间表编号
	 */
	@Id
	private Integer userAddonId;
	/**
	 * 用户编号
	 */
	private String userId;
	/**
	 * 用户硬件编号
	 */
	private Integer addonId;
	/**
	 * 价格
	 */
	private Double price;
	/**
	 * 备注
	 */
	private String note;
	/**
	 *  1=续约，0=不续约
	 */
	private Integer isRenewed;
	/**
	 * 是否试用，1=试用，2=不试用
	 */
	private Integer isTrial;
	/**
	 * 开始日期
	 */
	private LocalDate startDate;
	/**
	 * 结束日期
	 */
	private LocalDate stopDate;

}
