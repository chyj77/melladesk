package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 用户（医院工作人员）和工作单位的中间表，注意这里的用户是指的医院工作人员和工作单位的绑定关系。
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:12
 */
@Data
@Table("user__workplace")
public class UserWorkplaceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户工作单位中间表编号
	 */
	@Id
	private Integer userWorkplaceId;
	/**
	 * 工作单位编号
	 */
	private Integer workplaceId;
	/**
	 * 医生编号=用户编号
	 */
	private String userId;
	/**
	 * 是否启用，1=启用，0未启用
	 */
	private Integer isEnabled;
	/**
	 * 创建时间
	 */
	private LocalDate createTime;
	/**
	 * 创建绑定的人
	 */
	private String createBy;

}
