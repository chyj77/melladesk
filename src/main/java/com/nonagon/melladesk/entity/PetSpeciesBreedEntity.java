package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 宠物的品种表，例如猫物种下有很多品种，金吉拉，渐层，英短，美短，布偶猫等
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:13
 */
@Data
@Table("pet_species_breed")
public class PetSpeciesBreedEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 宠物物种的品种
	 */
	@Id
	private Integer petSpeciesBreedId;
	/**
	 * 物种编号
	 */
	private Integer speciesId;
	/**
	 * 宠物的品种名
	 */
	private String breedName;
	/**
	 * 品种详细描述
	 */
	private String breedDescription;

}
