package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:14
 */
@Data
@Table("organization")
public class OrganizationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 组织id
	 */
	@Id
	private Integer organizationId;
	/**
	 * 组织名称
	 */
	private String name;
	/**
	 * APIkey
	 */
	private String connectionKey;
	/**
	 * 集成地址
	 */
	private String connectionUrl;
	/**
	 * 集成方法 vetspire等等
	 */
	private String integrationMethod;

}
