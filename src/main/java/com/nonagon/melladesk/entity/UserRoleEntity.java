package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户表和角色表的中间表，记录用户和角色的关系，可以一个用户对应多个角色
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:12
 */
@Data
@Table("user__role")
public class UserRoleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户角色编号
	 */
	@Id
	private Integer userRoleId;
	/**
	 * 用户编号
	 */
	private String userId;
	/**
	 * 角色编号
	 */
	private Integer roleId;
	/**
	 * 上次工作单位的编号
	 */
	private Integer lastWorkplaceId;

}
