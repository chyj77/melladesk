package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 图片表，记录图片的信息
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:15
 */
@Data
@Table("image")
public class ImageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 图片编号
	 */
	@Id
	private Integer imageId;
	/**
	 * 图片种类
	 */
	private String type;
	/**
	 * 文件名
	 */
	private String fileName;
	/**
	 * 地址
	 */
	private String url;

}
