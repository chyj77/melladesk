package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 关于宠物的所有的关联，这里记录了这个宠物和主人，医院，集成系统的宠物编号，集成系统的主人的编号
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:14
 */
@Data
@Table("pet__all")
public class PetAllEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 宠物联系
	 */
	@Id
	private Integer petAllId;
	/**
	 * 宠物编号
	 */
	private String petId;
	/**
	 * 用户编号
	 */
	private String userId;
	/**
	 * 组织编号
	 */
	private Integer organizationId;
	/**
	 * 宠物集成编号
	 */
	private String patientId;
	/**
	 * 主人集成编号
	 */
	private String clientId;
	/**
	 * 解除绑定的时间
	 */
	private LocalDate disabledTime;
	/**
	 * 创建时间
	 */
	private LocalDate createTime;
	/**
	 * 创建人
	 */
	private String createBy;

}
