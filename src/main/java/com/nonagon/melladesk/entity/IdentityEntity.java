package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 用户身份表，用户的mella，facebook，google，apple四种登陆方式
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:15
 */
@Data
@Table("identity")
public class IdentityEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 身份编号
	 */
	@Id
	private Integer identityId;
	/**
	 * 哈希字符串
	 */
	private String hash;
	/**
	 * 是否启用，1=启用，2=不启用
	 */
	private Integer isEnabled;
	/**
	 * 创建时间
	 */
	private LocalDate createTime;
	/**
	 * 更新时间
	 */
	private LocalDate updateTime;

}
