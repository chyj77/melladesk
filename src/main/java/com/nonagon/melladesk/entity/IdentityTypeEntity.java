package com.nonagon.melladesk.entity;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

/**
 * 身份种类表
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:15
 */
@Data
@Table("identity_type")
public class IdentityTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@Id
	private Integer identityTypeId;
	/**
	 * 1=mella，2=goggle，3=facebook，4=apple
	 */
	private String identityTypeName;

}
