package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 硬件设备表，用来记录硬件设备的一些基本信息
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:14
 */
@Data
@Table("device")
public class DeviceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 硬件编号
	 */
	@Id
	private Integer deviceId;
	/**
	 * 医院编号
	 */
	private Integer workplaceId;
	/**
	 * 用户编号
	 */
	private String userId;
	/**
	 * 硬件的UUID
	 */
	private String uuid;
	/**
	 * 硬件的mac地址
	 */
	private String macAddr;
	/**
	 * 硬件描述
	 */
	private String description;

}
