package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 物种和病例种类的中间表
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:13
 */
@Data
@Table("species__vital_type")
public class SpeciesVitalTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 物种和不同病例（耳温，腋温，肛温）对应温度的关系表
	 */
	@Id
	private Integer speciesVitalTypeId;
	/**
	 * 宠物物种编号
	 */
	private Integer petSpeciesId;
	/**
	 * 宠物病例编号
	 */
	private Integer petVitalTypeId;
	/**
	 * 高温
	 */
	private Double high;
	/**
	 * 正常
	 */
	private Double normal;
	/**
	 * 低温
	 */
	private Double low;

}
