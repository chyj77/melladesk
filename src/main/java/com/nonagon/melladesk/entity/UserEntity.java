package com.nonagon.melladesk.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 用户表，包含宠物主人，宠物医生，宠物护士，宠物医院管理者的详细的基本信息
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table("user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户编号
	 */
	@Id
	private String userId;
	/**
	 * 设置编号
	 */
	private Integer userSettingId;
	/**
	 * 图片编号
	 */
	private Integer imageId;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 手机号码
	 */
	private String phone;
	/**
	 * 名
	 */
	private String firstName;
	/**
	 * 姓
	 */
	private String lastName;
	/**
	 * 国家
	 */
	private String country;
	/**
	 * 城市
	 */
	private String city;
	/**
	 * 州
	 */
	private String state;
	/**
	 * 邮政编码
	 */
	private Integer zipcode;
	/**
	 * 地址1
	 */
	private String address1;
	/**
	 * 
	 */
	private String address2;
	/**
	 * 是否被删除，1删除，0未删除
	 */
	private Integer isDeleted;
	/**
	 * 1=封停账号，0=未封号
	 */
	private Integer isLimit;
	/**
	 * 创建时间
	 */
	private LocalDate createTime;


}
