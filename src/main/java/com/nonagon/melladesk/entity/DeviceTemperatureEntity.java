package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 硬件设备的记录表，记录了该设备的一些测量信息以及数据
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:14
 */
@Data
@Table("device_temperature")
public class DeviceTemperatureEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 设备温度编号
	 */
	@Id
	private Integer deviceTemperatureId;
	/**
	 * 工作单位编号
	 */
	private Integer workplaceId;
	/**
	 * 用户编号
	 */
	private String userId;
	/**
	 * 宠物病人编号
	 */
	private String patientId;
	/**
	 * 宠物主人编号
	 */
	private String clientId;
	/**
	 * 宠物的物种编号
	 */
	private Integer petSpeciesBreedId;
	/**
	 * 设备编号
	 */
	private Integer deviceId;
	/**
	 * 测量部位编号
	 */
	private Integer petVitalTypeId;
	/**
	 * 测量的温度
	 */
	private Double data;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 创建时间
	 */
	private LocalDate createTime;
	/**
	 * 更改时间
	 */
	private LocalDate updateTime;

}
