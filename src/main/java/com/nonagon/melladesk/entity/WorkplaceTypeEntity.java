package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 工作单位的类型，如收容所，宠物医院，宠物诊所
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:15
 */
@Data
@Table("workplace_type")
public class WorkplaceTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 工作单位种类,Practice=兽医医院，Clinic=诊所，Shelter=收容所
	 */
	@Id
	private Integer workplaceTypeId;
	/**
	 * 工作单位的种类名称
	 */
	private String name;

}
