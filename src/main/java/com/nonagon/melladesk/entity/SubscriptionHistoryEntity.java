package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 订阅的插件以及活动的历史表。
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:13
 */
@Data
@Table("subscription_history")
public class SubscriptionHistoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 订阅历史表
	 */
	@Id
	private Integer subscriptionHistoryId;
	/**
	 * 用户订阅硬件
	 */
	private Integer userSubscriptionAddonId;
	/**
	 * 价格
	 */
	private Double price;
	/**
	 * 发票号码，如果他们有一个第三方计费系统，该系统将自动支付此费用。
	 */
	private String externalId;
	/**
	 * 是否支付，1=支付，0=未支付
	 */
	private Integer isPaid;
	/**
	 * 开始日期
	 */
	private LocalDate startDate;
	/**
	 * 结束日期
	 */
	private LocalDate endDate;

}
