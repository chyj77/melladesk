package com.nonagon.melladesk.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

/**
 * 
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:11
 */
@Data
@Table("clinical_data")
public class ClinicalDataEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 临床测试数据编号
	 */
	@Id
	private Integer clinicalDataId;
	/**
	 * 临床测试环境编号
	 */
	private Integer clinicalDatagroupId;
	/**
	 * 探头编号
	 */
	private Integer probeId;
	/**
	 * 测量部位种类
	 */
	private Integer petVitalTypeId;
	/**
	 * 
	 */
	private Integer ad0;
	/**
	 * 前端传感器
	 */
	private Double data0;
	/**
	 * 
	 */
	private Integer ad1;
	/**
	 * 中部传感器
	 */
	private Double data1;
	/**
	 * 
	 */
	private Integer ad2;
	/**
	 * 板载传感器
	 */
	private Double data2;
	/**
	 * 
	 */
	private String createTime;
	/**
	 * 是否正确，0不正确，1正确
	 */
	private Integer isNormal;

}
