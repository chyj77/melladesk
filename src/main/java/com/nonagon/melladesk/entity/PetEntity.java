package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 宠物表，记录宠物的姓名，生日，性别等基本信息
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:14
 */
@Data
@Table("pet")
public class PetEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 宠物编号
	 */
	@Id
	private String petId;
	/**
	 * 照片编号
	 */
	private Integer imageId;
	/**
	 * 物种_品种编号
	 */
	private Integer petSpeciesBreedId;
	/**
	 * 宠物姓名
	 */
	private String petName;
	/**
	 * 宠物生日
	 */
	private LocalDate birthday;
	/**
	 * 性别，0=公猫，1=母猫
	 */
	private Integer gender;
	/**
	 * 体重
	 */
	private Double weight;
	/**
	 * 是否绝育，1=绝育，0=未绝育
	 */
	private Integer isNeutered;
	/**
	 * 逻辑删除字段,1为删除，0为未删除
	 */
	private Integer disabled;
	/**
	 * 创建时间
	 */
	private LocalDate createTime;

}
