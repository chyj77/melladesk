package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户的角色表，宠物主人，宠物医生，宠物护士，宠物医院管理者
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:15
 */
@Data
@Table("role")
public class RoleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 角色名字
	 */
	@Id
	private Integer roleId;
	/**
	 * 角色名称，1宠物主人/宠物护士，2宠物医生，3宠物医院管理员
	 */
	private String roleName;
	/**
	 * 权限框架名称
	 */
	private String name;

}
