package com.nonagon.melladesk.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

/**
 * 品种和病例种类的中间表
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:12
 */
@Data
@Table("breed__vital_type")
public class BreedVitalTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品种和病例种类对应的高低温编号
	 */
	@Id
	private Integer breedVitalTypeId;
	/**
	 * 宠物品种编号
	 */
	private Integer petSpeciesBreedId;
	/**
	 * 宠物病例编号
	 */
	private Integer petVitalTypeId;
	/**
	 * 高温
	 */
	private Double high;
	/**
	 * 正常温度
	 */
	private Double normal;
	/**
	 * 低温
	 */
	private Double low;

}
