package com.nonagon.melladesk.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

/**
 * 订阅插件表，用来记录用户订阅插件的详细信息
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:14
 */
@Data
@Table("addon")
public class AddonEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户订阅插件
	 */
	@Id
	private Integer addonId;
	/**
	 * 描述
	 */
	private String description;
	/**
	 * 价格
	 */
	private Double price;

}
