package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

/**
 * 
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:11
 */
@Data
@Table("clinical_datagroup")
public class ClinicalDatagroupEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 临床测试编号
	 */
	@Id
	private Integer clinicalDatagroupId;
	/**
	 * 用户主人编号
	 */
	private String ownerId;
	/**
	 * 宠物医生编号
	 */
	private String doctorId;
	/**
	 * 第三方宠物编号
	 */
	private String petId;
	/**
	 * 医院工作单位编码
	 */
	private Integer workplaceId;
	/**
	 * 包间隔时间
	 */
	private Integer packageInterval;
	/**
	 * 采样速率
	 */
	private Integer sampleRate;
	/**
	 * 创建时间
	 */
	private LocalDate createTime;
	/**
	 * 备忘录
	 */
	private String memo;

}
