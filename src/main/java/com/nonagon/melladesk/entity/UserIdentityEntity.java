package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户表和用户身份表的中间表，一个用户可以对应多个身份（apple,mella,facebook,google）登陆
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:12
 */
@Data
@Table("user__identity")
public class UserIdentityEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户身份编号
	 */
	@Id
	private Integer userIdentityId;
	/**
	 * 用户编号
	 */
	private String userId;
	/**
	 * 身份编号
	 */
	private Integer identityId;
	/**
	 * 登陆种类1=mella，2=goggle，3=facebook，4=apple
	 */
	private Integer identityTypeId;

}
