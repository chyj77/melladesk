package com.nonagon.melladesk.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import java.io.Serializable;

/**
 * 集成系统的映射，例如VetSpire中的宠物体重对应mella的宠物体重，vetspire.weight=mella.weight
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:14
 */
@Data
@Table("integration_parameter")
public class IntegrationParameterEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 集成映射编号
	 */
	@Id
	private Integer integrationParameterId;
	/**
	 * 组织编号
	 */
	private Integer organizationId;
	/**
	 * 集成的参数名
	 */
	private String parameterName;
	/**
	 * 集成的参数值
	 */
	private String parameterValue;

}
