package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 工作单位表（宠物医院，宠物诊所，宠物收容所），记录工作单位的具体信息
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:12
 */
@Data
@Table("workplace")
public class WorkplaceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 工作单位编号
	 */
	@Id
	private Integer workplaceId;
	/**
	 * 组织id
	 */
	private Integer organizationId;
	/**
	 * 工作单位种类编号
	 */
	private Integer workplaceTypeId;
	/**
	 * 工作单位名称
	 */
	private String workplaceName;
	/**
	 * 地址1
	 */
	private String address1;
	/**
	 * 地址2
	 */
	private String address2;
	/**
	 * 手机号码
	 */
	private String phone;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 国家
	 */
	private String country;
	/**
	 * 城市
	 */
	private String city;
	/**
	 * 州
	 */
	private String state;
	/**
	 * 邮政编码
	 */
	private Integer zipcode;

}
