package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户日志表，记录用户进行的操作的一些相关信息
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:12
 */
@Data
@Table("user_log")
public class UserLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户日志表
	 */
	@Id
	private Integer userLogId;
	/**
	 * 用户编号
	 */
	private String userId;
	/**
	 * 描述
	 */
	private String description;
	/**
	 * 用户操作对应的表名
	 */
	private String objectName;
	/**
	 * 用户操作对应的列的名
	 */
	private String objectId;

}
