package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 宠物的病例表，记录着宠物主人，宠物医生，工作单位，宠物等病例的基本信息。
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:13
 */
@Data
@Table("pet_vital")
public class PetVitalEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 宠物病例编号
	 */
	@Id
	private String petVitalId;
	/**
	 * 宠物编号
	 */
	private String petId;
	/**
	 * 宠物种类编号
	 */
	private Integer petVitalTypeId;
	/**
	 * 宠物集成编号
	 */
	private String patientId;
	/**
	 * 带宠物主人看病的用户编号
	 */
	private String masterId;
	/**
	 * 给宠物主人诊断的用户编号
	 */
	private String doctorId;
	/**
	 * 工作单位编号
	 */
	private Integer workplaceId;
	/**
	 * 数据值
	 */
	private Double data;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 是否上载，1=上载，0未上载
	 */
	private Integer isUploaded;
	/**
	 * 设备编号号码
	 */
	private String deviceNumber;
	/**
	 * 创建日期
	 */
	private LocalDate createTime;
	/**
	 * 修改日期
	 */
	private LocalDate modifyTime;

}
