package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 宠物病例的种类表，耳温，腋温，肛温等
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:13
 */
@Data
@Table("pet_vital_type")
public class PetVitalTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 宠物病例种类
	 */
	@Id
	private Integer petVitalTypeId;
	/**
	 * 病例种类的图片
	 */
	private Integer imageId;
	/**
	 * 方法名称，耳温，腋温，肛温
	 */
	private String methodName;

}
