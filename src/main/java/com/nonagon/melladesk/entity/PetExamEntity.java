package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Table("pet_exam")
public class PetExamEntity implements Serializable {

    @Id
    private Integer examId;

    private String petId;

    private String userId;

    private String doctorId;

    private Integer deviceId;

    private Integer workplaceId;

    private Integer petVitalTypeId;

    private Double temperature;

    private String memo;

    private LocalDate createTime;

}
