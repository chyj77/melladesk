package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户设置表，记录用户是否打开或关闭温度警告通知，以及测量信息英制还是公制
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-02-02 14:37:46
 */
@Data
@Table("user_setting")
public class UserSettingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 设置编号
	 */
	@Id
	private Integer settingId;
	/**
	 * 版本号
	 */
	private Integer version;
	/**
	 * 温度单位，0=华氏度，1=摄氏度
	 */
	private Integer temperatureUnit;
	/**
	 * 背光时间
	 */
	private Integer backlightTime;
	/**
	 * 自动关闭时间
	 */
	private Integer autocloseTime;
	/**
	 * 提示音，0=开，1=关。
	 */
	private Integer warningTone;
	/**
	 * 温度警示，1=警示，0=不警示
	 */
	private Integer isWarning;
	/**
	 * 是否公制，1=公制，0=英制
	 */
	private Integer isMetric;
	/**
	 * 备注
	 */
	private String note;

}
