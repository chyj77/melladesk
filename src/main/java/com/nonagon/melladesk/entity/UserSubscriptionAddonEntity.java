package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 用户表和订阅表和插件表 ，三张表之间的关联表
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:12
 */
@Data
@Table("user__subscription__addon")
public class UserSubscriptionAddonEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户和订阅和插件中间编号
	 */
	@Id
	private Integer userSubscriptionAddonId;
	/**
	 * 用户编号
	 */
	private String userId;
	/**
	 * 订阅编号
	 */
	private Integer subscriptionId;
	/**
	 * 硬件编号
	 */
	private Integer addonId;
	/**
	 * 下次付费日期
	 */
	private LocalDate nextBillingDate;
	/**
	 * 下次付费的价格
	 */
	private Double nextBillingPrice;
	/**
	 * 是否试用
	 */
	private Integer isTrial;
	/**
	 * 备注
	 */
	private String note;

}
