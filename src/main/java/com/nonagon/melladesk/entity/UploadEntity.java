package com.nonagon.melladesk.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author flitsneak
 * @ClassName
 * @date 2021/3/17 17:37
 */
@Data
public class UploadEntity implements Serializable {

    private String domain;

    private String md5;

    private Long mtime;

    private String path;

    private Integer retcode;

    private String retmsg;

    private String scene;

    private String scenes;

    private long size;

    private String src;

    private String url;

}
