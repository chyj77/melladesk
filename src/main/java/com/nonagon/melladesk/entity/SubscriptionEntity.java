package com.nonagon.melladesk.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 用户订阅表，用户订阅的一些活动内容等
 * 
 * @author flitsneak
 * @email flitsneak@gmail.com
 * @date 2021-01-26 17:39:15
 */
@Data
@Table("subscription")
public class SubscriptionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 订阅计划编号
	 */
	@Id
	private Integer subscriptionId;
	/**
	 * 描述
	 */
	private String description;
	/**
	 * 期数
	 */
	private Integer term;
	/**
	 * 价格
	 */
	private Double price;
	/**
	 * 开始日期
	 */
	private LocalDate startTime;
	/**
	 * 结束日期
	 */
	private LocalDate endTime;

}
