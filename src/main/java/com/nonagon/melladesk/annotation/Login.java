package com.nonagon.melladesk.annotation;

import java.lang.annotation.*;

/**
 * 自定义登录权限注解用于AOP登录权限验证
 * @ClassName:  LoginAOP
 * @Author: flitsneak nonagon
 * @Date: 2020-12-7
 */
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
@Documented
public @interface Login {
  //String Module[] authorities() default {"login"};
}
