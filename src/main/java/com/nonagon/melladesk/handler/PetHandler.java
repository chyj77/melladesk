package com.nonagon.melladesk.handler;

import com.nonagon.melladesk.repository.PetRepository;
import com.nonagon.melladesk.utils.R;
import com.nonagon.melladesk.utils.StatusCode;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.server.ServerResponse.notFound;

/**
 * @author flitsneak
 * @ClassName
 * @date 2021/3/23 13:56
 */
@Component
@RequiredArgsConstructor
public class PetHandler {

    private final PetRepository petRepository;

    //测试样例，获取所有宠物信息
    public Mono<ServerResponse> getPetInfo(ServerRequest serverRequest){
        return petRepository.findByPetId(serverRequest.pathVariable("petId"))
                .flatMap(p-> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(new R(true, StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMsg(),p)))
                .switchIfEmpty(notFound().build());
    }
}
