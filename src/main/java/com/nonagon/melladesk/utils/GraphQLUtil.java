package com.nonagon.melladesk.utils;

import org.mountcloud.graphql.GraphqlClient;
import org.mountcloud.graphql.request.mutation.DefaultGraphqlMutation;
import org.mountcloud.graphql.request.mutation.GraphqlMutation;
import org.mountcloud.graphql.request.query.DefaultGraphqlQuery;
import org.mountcloud.graphql.request.query.GraphqlQuery;

import java.util.HashMap;
import java.util.Map;

/**
 * @author flitsneak
 * @ClassName graphql工具 有兴趣可以改造成建造者模式
 * @date 2021/4/15 9:14
 */
public class GraphQLUtil {

    public static GraphqlQuery buildGraphQL(GraphqlClient graphqlClient,String queryName,String APIkey){
        //使用Map来存储放入请求头，Authorization的密钥
        Map<String, String> httpHeaders = new HashMap<>();
        //httpHeaders.put("Authorization", "SFMyNTY.g3QAAAACZAAEZGF0YW0AAAAIMTIxOjUxNDdkAAZzaWduZWRuBgA2HeSibwE.FSigojcmpREK5InTRp1iaxg_S2QQnz_Vd5_Z_PIlRQc");
        httpHeaders.put("Authorization",APIkey);
        //设置http请求头
        graphqlClient.setHttpHeaders(httpHeaders);
        //创建一个query并设置query的名字
        GraphqlQuery query = new DefaultGraphqlQuery(queryName);
        return query;
    }

    public static GraphqlMutation buildMutation(GraphqlClient graphqlClient,String queryName,String APIkey){
        Map<String, String> httpHeaders = new HashMap<>();
        httpHeaders.put("Authorization",APIkey);
        graphqlClient.setHttpHeaders(httpHeaders);
        GraphqlMutation mutation = new DefaultGraphqlMutation(queryName);
        return mutation ;
    }
}
