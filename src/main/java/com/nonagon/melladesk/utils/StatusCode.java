package com.nonagon.melladesk.utils;
/**
 * 规范后的状态返回码
 * @ClassName:  StatusCode
 * @author: flitsneak nonagon
 * @date:   2020-11-28
 */
/**
 * 定义规则为5位数字,前两位表示业务场景，后三位表示错误码。如100001，10为通用处理异常，000为系统未知异常。
 * 10：通用
 * 11：用户
 * 12：宠物
 * 13：测温仪器
 * 14:VetSpire
 * 20000 成功
 */
public enum StatusCode {

    ERROR(10000,"fail"),
    SUCCESS(20000,"success"),
    EMAILHASALREADYEXIST(11002,"邮箱已存在"),
    USERHAVENOTLOGIN(11006,"用户未登录"),
    QUERYFAIL(11003,"查询失败"),
    TOKENDONOTEXIST(11007,"没有令牌"),
    TOKENERROR(11008,"令牌错误"),
    LOGINERROR(11009,"登录错误"),
    TOKENFORGE(11025,"伪造的令牌"),
    TOKENISSUESUCESS(11010,"令牌签发成功"),
    USERHASALREADYEXIST(11011,"用户已存在"),
    USERHASLIMITED(11012,"账号被限制"),
    USERAVAIBLE(11013,"用户可用"),
    EMAILDONOTEXIST(11014,"邮箱账户不存在"),
    EMAILVERIFYFAIL(11015,"邮箱验证失败"),
    EMAILVERIFYSUCCESS(11016,"邮箱验证成功，你可以重置密码"),
    ACTIVESUCCESS(11005,"账号激活成功"),
    CODEVERIFICATION_FAIL(11004,"验证码验证失败"),
    EMAILHASNOTCONFIRM(11017,"修改密码的邮件未确认"),
    PHONECODEEXPIRED(11018,"手机验证码已过期"),
    PHONECODEERROR(11019,"手机验证码错误"),
    PHONEVERIFIEDSUCESS(11020,"手机验证成功"),
    EMAILLINKVERITYERROR(11021,"邮箱验证链接错误"),
    REDISEXPIRED(11022,"redis缓存过期"),
    RESETPASSWORDBYEMAILSUCCESS(11023,"通过邮箱修改密码成功"),
    THIRDPARTAUTHSUCCESS(11024,"第三方登录授权成功"),
    VETSPIREAPIKEYWRONG(14001,"APIkey错误"),
    VETSPIRESUCCESS(14002,"VetSpire请求成功"),
    UNKNOW_EXCEPTION(10000,"系统内部错误"),
    TOKENBLANKERROR(10001,"令牌空白错误"),
    ADDPETSUCCESS(12001,"宠物添加成功"),
    LOGICDELETEPETSUCCESS(12002,"逻辑删除宠物成功"),
    GETPETBYPETIDSUCCESS(12003,"根据petid获取宠物信息成功"),
    GETPETBYPETIDFAIL(12004,"根据petid获取宠物信息失败"),
    PETEXAMLISTSUCCESS(12005,"宠物检查表单查询成功"),
    PETEXAMLISTBYHISTORYARRAYSUCCESS(12006,"通过历史数组查询宠物表单成功"),
    PETEXAMUPDATEFAIL(12007,"更新宠物检查表单成功"),
    ;

    private int code;
    private String msg;

    StatusCode(int code, String msg){
        this.code=code;
        this.msg=msg;
    }

    public int getCode(){
        return code;
    }
    public String getMsg(){
        return msg;
    }

}
