package com.nonagon.melladesk.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author flitsneak
 * @ClassName
 * @date 2021/4/2 17:11
 */
@Service
@RequiredArgsConstructor
public class RedisService {

    private final ReactiveRedisTemplate<String,String> reactiveRedisTemplate;

    public void saveToken(String token){
        reactiveRedisTemplate.opsForSet().add("token_set",token)
                .subscribe();
    }

}
