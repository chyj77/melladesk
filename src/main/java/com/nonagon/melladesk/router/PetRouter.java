package com.nonagon.melladesk.router;

import com.nonagon.melladesk.handler.PetHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

/**
 * @author flitsneak
 * @ClassName 宠物路由
 * @date 2021/3/23 13:50
 */
@Configuration
public class PetRouter {
    @Bean
    public RouterFunction<ServerResponse> petRouterFunction(PetHandler petHandler){
        return RouterFunctions.route(GET("/pet/{petId}").and(accept(APPLICATION_JSON)),petHandler::getPetInfo);
    }
}
