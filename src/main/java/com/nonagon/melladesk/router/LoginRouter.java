package com.nonagon.melladesk.router;

import com.nonagon.melladesk.handler.UserHandler;
import com.nonagon.melladesk.vo.UserDetail;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springdoc.core.annotations.RouterOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;

/**
 * @author flitsneak
 * @ClassName 登录路由
 * @date 2021/4/6 10:18
 */
@Configuration
public class LoginRouter {
    @RouterOperation(operation = @Operation(operationId = "login",summary = "just login",tags = {"login"},
            responses = { @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = UserDetail.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid"),
                    @ApiResponse(responseCode = "404", description = "not found")}

    ))
    @Bean
    public RouterFunction<ServerResponse> userRouterFunction(UserHandler userHandler){
        return RouterFunctions.route(POST("/auth/login").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)),userHandler::login);
    }
}
