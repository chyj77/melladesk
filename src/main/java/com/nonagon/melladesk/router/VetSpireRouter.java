package com.nonagon.melladesk.router;

import com.nonagon.melladesk.entity.PetEntity;
import com.nonagon.melladesk.handler.VetSpireHandler;
import org.springdoc.core.annotations.RouterOperation;
import org.springdoc.core.annotations.RouterOperations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;

/**
 * @author flitsneak
 * @ClassName 美方兽医网 vetspire对接路由
 * @date 2021/4/12 9:43
 */
@Configuration
public class VetSpireRouter {
    @RouterOperations({
            @RouterOperation(path = "/selectLocationsByOrganization", beanMethod = "selectLocationsByOrganization"),
            @RouterOperation(path = "/VetSpire/selectLocations", beanMethod = "selectLocations"),
            @RouterOperation(path = "/VetSpire/selectProvidersByLocationId",  beanMethod = "selectProvidersByLocationId"),
            @RouterOperation(path = "/VetSpire/selectAllPets",  beanMethod = "selectAllPets") })
    @Bean
    public RouterFunction<ServerResponse> vetSpireRouterFunction(VetSpireHandler vetSpireHandler){
        return RouterFunctions.route(POST("/VetSpire/selectLocationsByOrganization").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)),vetSpireHandler::selectLocationsByOrganization)
                .andRoute(POST("/VetSpire/selectLocations").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)),vetSpireHandler::selectLocations)
                .andRoute(POST("/VetSpire/selectProvidersByLocationId").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)),vetSpireHandler::selectProvidersByLocationId)
                .andRoute(POST("/VetSpire/selectAllPets").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)),vetSpireHandler::selectAllPets)
                .andRoute(POST("/VetSpire/selectExamByPatientId").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)),vetSpireHandler::selectExamByPatientId)
                .andRoute(POST("/VetSpire/updateNoteByVitalId").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)),vetSpireHandler::updateNoteByVitalId)
                .andRoute(POST("/VetSpire/selectPatientsByProviderId").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)),vetSpireHandler::selectPatientsByProviderId)
                .andRoute(POST("/VetSpire/selectLatestExamsByPatientId").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)),vetSpireHandler::selectLatestExamsByPatientId)
                .andRoute(POST("/VetSpire/selectHistoryExamsByPatientId").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)),vetSpireHandler::selectHistoryExamsByPatientId)
                .andRoute(POST("/VetSpire/updateVitalsTemperatureByVitalId").and(accept(APPLICATION_JSON)).and(contentType(APPLICATION_JSON)),vetSpireHandler::updateVitalsTemperatureByVitalId)
                ;
    }
}
