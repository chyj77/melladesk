package com.nonagon.melladesk.repository;

import com.nonagon.melladesk.entity.RoleEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * @author flitsneak
 * @ClassName
 * @date 2021/3/22 18:08
 */
public interface RoleRepository extends ReactiveCrudRepository<RoleEntity,Integer> {
}
