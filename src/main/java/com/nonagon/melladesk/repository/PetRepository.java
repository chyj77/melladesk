package com.nonagon.melladesk.repository;

import com.nonagon.melladesk.entity.PetEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

/**
 * @author flitsneak
 * @ClassName
 * @date 2021/4/2 17:40
 */
public interface PetRepository extends ReactiveCrudRepository<PetEntity,String> {

    Mono<PetEntity> findByPetId(String petId);
}
