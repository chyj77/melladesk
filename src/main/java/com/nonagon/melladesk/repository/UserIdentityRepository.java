package com.nonagon.melladesk.repository;

import com.nonagon.melladesk.entity.UserIdentityEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

/**
 * @author flitsneak
 * @ClassName
 * @date 2021/3/23 10:00
 */
public interface UserIdentityRepository extends ReactiveCrudRepository<UserIdentityEntity,Integer> {

    Mono<UserIdentityEntity> findByIdentityTypeIdAndUserId(Integer identityTypeId,String userId);
}
