package com.nonagon.melladesk.repository;

import com.nonagon.melladesk.entity.UserRoleEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

/**
 * @author flitsneak
 * @ClassName
 * @date 2021/3/22 16:27
 */
public interface UserRoleRepository extends ReactiveCrudRepository<UserRoleEntity,Integer> {

    Flux<UserRoleEntity> findAllByUserId(String userId);
}
