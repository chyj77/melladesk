package com.nonagon.melladesk.repository;

import com.nonagon.melladesk.entity.UserEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

/**
 * @author flitsneak
 * @ClassName
 * @date 2021/3/22 16:24
 */
public interface UserRepository extends ReactiveCrudRepository<UserEntity,String> {

    Mono<UserEntity> findAllByEmail(String email);
}
