package com.nonagon.melladesk.repository;

import com.nonagon.melladesk.entity.IdentityEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * @author flitsneak
 * @ClassName
 * @date 2021/3/23 10:10
 */
public interface IdentityRepository extends ReactiveCrudRepository<IdentityEntity,Integer> {
}
