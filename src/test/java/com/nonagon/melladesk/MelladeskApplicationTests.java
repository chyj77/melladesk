package com.nonagon.melladesk;

import com.nonagon.melladesk.annotation.Login;
import com.nonagon.melladesk.entity.UserEntity;
import com.nonagon.melladesk.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Mono;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SpringBootTest
class MelladeskApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void findByEmailTest(){
        Mono<UserEntity> byEmail = userRepository.findById("1").cast(UserEntity.class);
        byEmail.subscribe(u-> System.out.println(u.getEmail()));
        Mono.just("hello").subscribe(System.out::println);
    }
    @Test
    void regexTest() throws ClassNotFoundException {
        String path = "http://192.168.0.36:18080/melladesk/VetSpire/updateVitalsTemperatureByVitalId";
        String regex = "(?<=melladesk/)(.*)(?=/)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(path);
        System.out.println(regex);
        matcher.find();
        String s = matcher.group();
        System.out.println(s);
        String className = "com.nonagon.melladesk.handler."+s + "Handler";
        Class<?> clazz = Class.forName(className);
        Method[] methods = clazz.getMethods();
        ArrayList<Method> updateVitalsTemperatureByVitalId = Arrays.stream(methods).filter(x -> x.toString().contains("updateVitalsTemperatureByVitalId"))
                .collect(Collectors.toCollection(ArrayList::new));
        updateVitalsTemperatureByVitalId.forEach(System.out::println);
        boolean b = updateVitalsTemperatureByVitalId.get(0).isAnnotationPresent(Login.class);
        System.out.println(b);

    }
}

